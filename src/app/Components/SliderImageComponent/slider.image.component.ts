import {Component, Input, OnInit} from "@angular/core";

@Component({
  selector: 'app-sliderimage',
  templateUrl: './slider.image.component.html',
  styleUrls: ['./slider.image.component.scss']
})
export class SliderImageComponent implements OnInit{
  @Input()background: any;

  ngOnInit(): void {
  }
}
