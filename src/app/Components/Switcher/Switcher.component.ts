import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'switcher',
  templateUrl: './Switcher.component.html',
  styleUrls: ['./Switcher.component.scss']
})
export class SwitcherComponent {
  @Input() entry: boolean = false;
  @Output() onchange: EventEmitter<boolean> = new EventEmitter();
  toggle(entry: boolean){
    if (this.entry !== entry) {
      this.entry = entry;
      this.onchange.emit(entry);
    }
  }
}
