import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginPage} from "./Pages/login/login.page";
import {DashboardPage} from "./Pages/dashboard/dashboard.page";
import {BillcardComponent} from "./Pages/Billcard/billcard.component";
import {GamingComponent} from "./Pages/gaming/gaming.component";
import {HeaderbarComponent} from "./Pages/headerbar/headerbar.component";
import {SpaceloginComponent} from "./Pages/spacelogin/spacelogin.component";

const routes: Routes = [
  {path: "" , redirectTo: "spacelogin", pathMatch: "full"},
  {path: "login", component: LoginPage},
  {path: "dashboard", component: DashboardPage},
  {path: "billcard", component: BillcardComponent},
  {path: "gaming", component: GamingComponent},
  {path: "headerbar", component: HeaderbarComponent},
  {path: "spacelogin", component: SpaceloginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
