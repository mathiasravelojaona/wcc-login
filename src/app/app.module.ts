import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {LoginPage} from "./Pages/login/login.page";
import {SliderImageComponent} from "./Components/SliderImageComponent/slider.image.component";
import {NgxUsefulSwiperModule} from "ngx-useful-swiper";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {UiStyleToggleService} from "./theme/UiStyleToggleService";
import {APP_INITIALIZER} from "@angular/core";
import {DashboardPage} from "./Pages/dashboard/dashboard.page";
import {SwitcherComponent} from "./Components/Switcher/Switcher.component";
import {BillcardComponent} from "./Pages/Billcard/billcard.component";
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import {MatSelectModule} from '@angular/material/select';
import {GamingComponent} from "./Pages/gaming/gaming.component";
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { HeaderbarComponent } from './Pages/headerbar/headerbar.component';
import { SpaceloginComponent } from './Pages/spacelogin/spacelogin.component';
import {MatInputModule} from "@angular/material/input";
import {MatFormField} from "@angular/material/form-field";
import {MatButtonModule} from "@angular/material/button";

export function themeFactory(themeService: UiStyleToggleService){
  return () => themeService.setThemeOnStart();
}
let options: Partial<IConfig> | (() => Partial<IConfig>);
@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    SliderImageComponent,
    DashboardPage,
    SwitcherComponent,
    BillcardComponent,
    GamingComponent,
    HeaderbarComponent,
    SpaceloginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxUsefulSwiperModule,
    ReactiveFormsModule,
    FormsModule,
    NgxMaskModule.forRoot(options),
    BrowserAnimationsModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatButtonModule
  ],
  providers: [
    UiStyleToggleService,
    {provide: APP_INITIALIZER, useFactory: themeFactory, deps: [UiStyleToggleService], multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
