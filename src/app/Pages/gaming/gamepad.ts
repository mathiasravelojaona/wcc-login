export interface Gamepad {
  color:string;
  stringcolor:string;
  background:string;
  enabled:boolean;
  transition:boolean;
  dice: string;
  vanishing: boolean;
  name: string;
  image: string;
  orb:string;
}
