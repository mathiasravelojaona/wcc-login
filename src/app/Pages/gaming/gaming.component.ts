import { Component, OnInit } from '@angular/core';
import {Gamepad} from "./gamepad";

@Component({
  selector: 'app-gaming',
  templateUrl: './gaming.component.html',
  styleUrls: ['./gaming.component.scss']
})
export class GamingComponent implements OnInit {

  enablednumber: number = 1;
  onloading: boolean = false;
  selectedone: number = 0;
  focusedOne: Gamepad;
  numbertab: any = {1: 'one', 2: 'two', 3: 'three', 4:'four'};
  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }
  focusOne(nombre: number){
    this.focusedOne = this.listegamepad[nombre];
  }
  defocusOne(nombre: number){
    this.focusedOne = null;
  }
  async changenumber(nombre: number){
    let taille = this.listegamepad.length;
    let newvalue = nombre;
    let depart = taille - 1;
    let arrivee = 0;
    this.onloading = true;
    for(let i = depart ; i >= arrivee ; i--){
      if(i>= newvalue){
        if(this.listegamepad[i].enabled){
          this.listegamepad[i].transition = true;
          await this.delay(800);
          this.listegamepad[i].enabled = false;
          this.listegamepad[i].transition = false;
        }
      }
      else{
        if(!this.listegamepad[i].enabled){
          this.listegamepad[i].transition = true;
          await this.delay(800);
          this.listegamepad[i].enabled = true;
          this.listegamepad[i].transition = false;
        }
      }
    }
    this.enablednumber = newvalue;
    this.onloading = false;
  }
  listegamepad: Gamepad[] = [{color:'#6200f3',stringcolor:'#5d8fda',background:'#a6cbfc',
    enabled: true, transition: false, dice:"dice-one", vanishing: false, name:"blue", image: '/assets/images/blue.jpg', orb:'fas fa-tint'},
    {color:'#ff0093',stringcolor:'#fb98c4',background:'#ffd8e8',enabled: false, transition: false, dice:"dice-two", vanishing: false, name:"pink", image: '/assets/images/red.png',orb:"fas fa-heart"},
    {color:'#00ff6c',stringcolor:'#4bffad',background:'#cdffe6',enabled: false, transition: false, dice:"dice-three", vanishing: false, name:"green", image: '/assets/images/green.jpg',orb:'fas fa-leaf'},
    {color:'#f9b445',stringcolor:'#f0ce86',background:'#f9ffe3',enabled: false, transition: false, dice:"dice-four", vanishing: false, name:"orange", image: '/assets/images/yellow.jpg',orb:'fas fa-fire'}];
  constructor() { }

  ngOnInit(): void {
  }

}
