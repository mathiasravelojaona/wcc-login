import {Component, NgZone, ChangeDetectorRef, ViewChild, ElementRef, AfterViewInit} from "@angular/core";
import {Divcardnumbers} from "./Divcardnumbers";
import {SwiperOptions} from "swiper";
import {SwiperComponent} from "ngx-useful-swiper";
import {first} from "rxjs/operators";
import * as $ from 'jquery';
@Component({
  selector: 'app-billcard',
  templateUrl: './billcard.component.html',
  styleUrls: ['./billcard.component.scss']
})
export class BillcardComponent implements AfterViewInit{
  config: SwiperOptions = {
    speed: 800,
    spaceBetween: 30,
    effect:'flip',
    simulateTouch: false
  };
  monthancientvalue: string = "MM";
  yearancientvalue: string = "YY";
  monthnewvalue: string = "MM";
  yearnewvalue: string = "YY";
  monthtransit: boolean = false;
  yeartransit: boolean = false;
  async reversecard(){
    $('#tomove').css('opacity',0);
    await this.usefulSwiper.swiper.slideTo(1);
  }
  async focuscard(){
    $('#tomove').css('opacity',0);
    await this.usefulSwiper.swiper.slideTo(0);
    this.blurIt('');
  }
  async changelogo(indice: number){
    this.transiting = true;
    this.indicebillcard = indice;
    await this.delay(150);
    this.indiceancientbillcard = indice;
    this.transiting = false;
  }

  @ViewChild('usefulswiper',{static: false}) usefulSwiper: SwiperComponent;
  cvv: string = "";
  indexmonth: string = '0';
  ancientclass: string = 'test';
  newclass: string = 'test';
  async toggleClass(classe: string){
      if(this.ancientclass == ''){
        this.blurIt(classe);
        await this.delay(700);
      }
      $('#tomove').css('opacity', 1);
      this.ancientclass = this.newclass;
      this.newclass = classe;
      this.changeToElement(classe);
  }
  blurIt(classe: string){
      $('#tomove').css('opacity',0);
      this.ancientclass = classe;
      this.newclass = classe;
      this.changeToElement('swipertest');
  }
  isClassActive(classe: string){
    return this.newclass == classe;
  }
  public customPatterns = { '0': { pattern: new RegExp('\[a-zA-Z\]')} };

  indexyear: string = '0';
  divcardnumbers: Array<Divcardnumbers[]> = [];
  mapdivcardnumbers: Map<number, Divcardnumbers> = new Map();
  cardtext: string =  "";
  listemonths = [{
    month: "MM",
    representation: "Month"
  }];
  listeyears = [{
    year: "YY",
    representation: "Year"
  }];
  indicebillcard: number = 0;
  indiceancientbillcard: number = 0;
  transiting: boolean = false;
  billcards = ['fa-cc-visa','fa-cc-mastercard','fa-cc-stripe','fa-paypal','fa-cc-jcb','fa-cc-discover','fa-cc-diners-club','fa-cc-apple-pay','fa-cc-amex','fa-cc-amazon-pay']
  public constructor(public zone: NgZone, public ref: ChangeDetectorRef) {
    this.initLists();
    this.getOtherHeight();
  }
  initLists(){
    let representation: string = null;
    for(let i = 1 ; i<=12 ; i++){
      representation = "" + i;
      representation = representation.length == 1 ? "0" + i : representation;
      this.listemonths.push({
        month: representation,
        representation
      });
    }
    let currentyear = new Date().getFullYear();
    let finalyear = currentyear + 11;
    for(let i = currentyear ; i<= finalyear ; i++){
      representation = "" + i;
      representation = representation.substr(2,2)
      this.listeyears.push({
        year: representation,
        representation: "" +i
      });
    }
    let temp_array: Array<Divcardnumbers> = [];
    let temp_value: Divcardnumbers = null;
    for(let i = 1; i<= 16; i++){
      temp_value = {
        ancientvalue: "#",
        newvalue: "#",
        ontransit: false
      };
      this.mapdivcardnumbers.set(i-1 , temp_value);
      temp_array.push(temp_value);
      if(i%4 == 0){
        this.divcardnumbers.push(temp_array);
        temp_array = [];
      }
    }
  }
  async updatecardsnormal(texte: string){
    let listecharacteres: string[] = texte.split('');
    while(listecharacteres.length != this.mapdivcardnumbers.size){listecharacteres.push('#')}

    let taillecharac = listecharacteres.length;
    let ancientvalue: Divcardnumbers = null;
    for(let i= 0; i< taillecharac ; i++){
      ancientvalue = this.mapdivcardnumbers.get(i);
      if(ancientvalue.ancientvalue != listecharacteres[i]){
        if(i == 0){
          let indice = Number(listecharacteres[i]);
          this.changelogo(indice);
        }
        ancientvalue.ontransit = true;
        ancientvalue.newvalue = "" + listecharacteres[i];
        await this.delay(150);
        ancientvalue.ancientvalue = ancientvalue.newvalue;
        ancientvalue.ontransit = false;
      }
    }
  }

  async updatecards(event, text: string){
    if(event.key === 'Delete' || event.key === "Backspace"){
      this.updatecardsondelete(text);
    }
    else{
      this.updatecardsnormal(text);
    }
  }
  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }
  firstTouch: boolean = false;
  name: string = "";
  async firstTouchCheck(){
    if(!this.firstTouch){
      this.firstTouch = true;
    }
  }
  getSimilarTexts(texte1: string, texte2: string){
    let tailletexte1: number = texte1.length;
    let tailletexte2: number = texte2.length;
    let firstdepart : boolean = tailletexte1 > tailletexte2;
    let textedepart: string = firstdepart ? texte2 : texte1;
  }
  changeToElement(className: string){
    let element = document.getElementsByClassName(className)[0];
    var rect = element.getBoundingClientRect();
    var rect2 = document.getElementsByClassName('swipertest')[0].getBoundingClientRect();
    $('#tomove').css('top', rect.top - rect2.top);
    $('#tomove').css('left',rect.left - rect2.left);
    $('#tomove').css('height',rect.height );
    $('#tomove').css('width',rect.width);
  }
  ngAfterViewInit(): void {
    setTimeout(() => {
      let nativeELement = document.getElementById('tomove');
      this.changeToElement('swipertest');
      nativeELement.classList.add('moving');
    }, 1000);
  }
  async updateMonth(event){
    let index = Number(event.value);
    this.monthtransit = true;
    let texte = this.listemonths[index].month;
    this.monthnewvalue = "" + texte;
    await this.delay(150);
    this.monthancientvalue = this.monthnewvalue;
    this.monthtransit = false;
  }
  async updateYear(event){
    let index = Number(event.value);
    this.yeartransit = true;
    let texte = this.listeyears[index].year;
    this.yearnewvalue = "" + texte;
    await this.delay(150);
    this.yearancientvalue = this.yearnewvalue;
    this.yeartransit = false;
  }
  async updatecardsondelete(texte: string){
    let listecharacteres: string[] = texte.split('');
    while(listecharacteres.length != this.mapdivcardnumbers.size){listecharacteres.push('#')}
    let taillecharac = listecharacteres.length;
    let ancientvalue: Divcardnumbers = null;
    for(let i= taillecharac - 1; i>=0 ; i--){

      ancientvalue = this.mapdivcardnumbers.get(i);
      if(ancientvalue.ancientvalue != listecharacteres[i]){
        if(i == 0){
          let indice = Number(listecharacteres[i]);
          this.changelogo(indice);
        }
        ancientvalue.ontransit = true;
        ancientvalue.newvalue = "" + listecharacteres[i];
        await this.delay(150);
        ancientvalue.ancientvalue = ancientvalue.newvalue;
        ancientvalue.ontransit = false;
      }
    }
  }
  otherHeight: number = 362;
  public getOtherHeight(){
    setTimeout(() => {
      this.otherHeight = document.getElementsByClassName('swipertest')[0].getBoundingClientRect().height;
    }, 1000);
  }
  validate(): boolean{
    return this.indexmonth!="0" && this.indexyear!="0" && this.cvv!='' && this.name!='' && this.cardtext.length == 16;
  }
}
