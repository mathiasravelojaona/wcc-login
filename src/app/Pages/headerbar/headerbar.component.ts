import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-headerbar',
  templateUrl: './headerbar.component.html',
  styleUrls: ['./headerbar.component.scss']
})
export class HeaderbarComponent implements OnInit {
  mode: boolean = false;
  ontransition: boolean = false;
  ongo: boolean = false;
  ongotest: boolean = false;
  texte: string = "";
  ondelete: boolean = false;
  hoveredPoints: boolean = false;
  constructor() { }
  @ViewChild('input', {static:false})input: ElementRef;

  async switchMode(mode: boolean){
   if(this.hoveredPoints){
     await this.toggleHover();
   }
    this.texte = "";
    await this.delay(275);
    this.ontransition = true;

    await this.delay(225);
    this.mode = mode;
    this.ontransition = false;
    setTimeout(async () => {
      await this.delay(50);
      this.ongo = mode;
    }, 100);

    if(mode){
      setTimeout(() => {
        this.input.nativeElement.focus();
      }, 100);
    }
    else{

    }
  }
  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }
  ngOnInit(): void {
  }
  async fadeCross(){
    this.ondelete = true;
    await this.delay(275);
    this.texte = "";
    this.ondelete = false;
  }
  onhoveredx: boolean = false;
  async toggleHover(){
    if(this.hoveredPoints){
      this.onhoveredx = true;
      await this.delay(300);
      this.onhoveredx = false;
    }
    this.hoveredPoints = !this.hoveredPoints;
  }
}
