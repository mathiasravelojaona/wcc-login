import {Component, OnInit, ViewChild} from "@angular/core";
import {SwiperOptions} from "swiper";
import {SwiperComponent} from "ngx-useful-swiper";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MustMatch} from "../../helper/helper";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit{
  config: SwiperOptions = {
    speed: 800,
    spaceBetween: 30,
    effect:'flip',
    autoplay: {
      delay: 3000,
      disableOnInteraction: false,
      stopOnLastSlide: false
    }
  };
  logForm: FormGroup;
  registerForm: FormGroup;
  configtabs: SwiperOptions = {
    speed: 800,
    spaceBetween: 30,
    effect: 'slide',
    simulateTouch: false
  }
  shown: boolean = false;
  current: number = 0;
  currenttab: number = 0;
  images =  [
    {image: '/assets/images/email.png', texte: 'Développez!'},
    {image: '/assets/images/mobile.png', texte: 'Sauvegardez'},
    {image: '/assets/images/online.png', texte: 'Déployez'}
  ];
  islogin: boolean = false;
  @ViewChild('usefulswiper',{static: false}) usefulSwiper: SwiperComponent;
  @ViewChild('tabswiper',{static: false}) tabswiper: SwiperComponent;
  ngOnInit(): void {
    setTimeout(() => {
      this.shown = true;
      setTimeout(() => {
        this.usefulSwiper.swiper.on('slideChangeTransitionStart', () => {
          this.current = this.usefulSwiper.swiper.realIndex;
        });
        this.tabswiper.swiper.on('slideChangeTransitionStart', () => {
          this.currenttab = this.tabswiper.swiper.realIndex;
        });
        this.logForm = this.formbuilder.group({
          email: ['', [Validators.required, Validators.email]],
          password: ['', [Validators.required]],
        });
        this.registerForm = this.formbuilder.group({
          email: ['', [Validators.required, Validators.email]],
          password: ['', [Validators.required, Validators.minLength(6)]],
          confirmPassword: ['', Validators.required],
          pseudo: ['', Validators.required]
        }, {
            validator: MustMatch('password', 'confirmPassword')
          });
      }, 800);
    }, 400);
  }
  createLogForm(){

  }
  async changeindex(i:number){
    this.current = i;
    await this.usefulSwiper.swiper.slideTo(i);
  }
  resetAll(){
    this.logForm.reset();
    this.registerForm.reset();
  }
  async changeTabIndex(i:number){
    this.resetAll();
    this.currenttab = i;
    await this.tabswiper.swiper.slideTo(i);
  }
  constructor(private formbuilder: FormBuilder){

  }
}
