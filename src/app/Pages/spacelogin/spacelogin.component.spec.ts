import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaceloginComponent } from './spacelogin.component';

describe('SpaceloginComponent', () => {
  let component: SpaceloginComponent;
  let fixture: ComponentFixture<SpaceloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpaceloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaceloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
