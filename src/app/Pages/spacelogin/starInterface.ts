export class StarInterface{
  top: number;
  left: number;
  size: number;
  value: string;
  title: string;
  ismeteor: boolean;
  issun: boolean;
  isbahai: boolean;
  activated: boolean;
}
