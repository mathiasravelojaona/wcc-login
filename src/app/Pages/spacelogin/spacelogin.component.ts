import { Component, OnInit } from '@angular/core';
import {StarInterface} from "./starInterface";

@Component({
  selector: 'app-spacelogin',
  templateUrl: './spacelogin.component.html',
  styleUrls: ['./spacelogin.component.scss']
})
export class SpaceloginComponent implements OnInit {
  activated: boolean = false;
  numberlist: number[] = [];
  numberlistbody: number[] = [];
  starlist: StarInterface[] = [];
  username: string = "";
  password: string = "";
  constructor() { }
  resetForAll(){
    this.username = "";
    this.password = "";
    this.resetAll();
  }
  ngOnInit(): void {
    this.username = "" ;
    this.password = "";
    for(let i= 0 ; i< 7 ; i++){
      this.numberlist.push(i);
    }
    for(let i= 0 ; i< 20 ; i++){
      this.numberlistbody.push(i);
    }
    this.starlist.push({top:5, left: 30, size:40, value:'.', title: "Alpha centauris",isbahai: true, issun: false, ismeteor: false,activated: false});
    this.starlist.push({top:5, left: 300, size:20, value:'.', title: "Milky way avenant",isbahai: false, issun: false, ismeteor: true,activated: false});
    this.starlist.push({top:75, left: 350, size:48, value:'.', title: "Star Seraph way",isbahai: true, issun: false, ismeteor: false,activated: false});
    this.starlist.push({top:100, left: 45, size:50, value:'.', title: "Mily way",isbahai: false, issun: true, ismeteor: false,activated: false});
    this.starlist.push({top:200, left: 360, size:20, value:'.', title: "Sunset Road",isbahai: false, issun: false, ismeteor: false,activated: false});
    this.starlist.push({top:250, left: 30, size:40, value:'.', title: "Jupiter's circle",isbahai: false, issun: false, ismeteor: true,activated: false});
    this.starlist.push({top:375, left: 50, size:90, value:'.', title: "Old Earth",isbahai: false, issun: true, ismeteor: false,activated: false});
    this.starlist.push({top:390, left: 360, size:40, value:'.', title: "New earth",isbahai: false, issun: false, ismeteor: false,activated: false});
  }
  resetAll(){
    this.starlist.forEach((star) => {
      this.defocusOne(star);
    });
  }
  defocusOne(star: StarInterface){
    if(!star.activated){
      return;
    }
    star.value = ".";
    star.size = star.size*2;
    star.activated = false;
  }
  focusOne(star: StarInterface){
    if(star.activated){
      return;
    }
    if(star.isbahai){
      star.value = "<i class='fas fa-bahai'></i>";
    }
    else if(star.ismeteor){
      star.value = "<i class='fas fa-meteor'></i>";
    }
    else if(star.issun){
      star.value = "<i class='fas fa-sun'></i>";
    }
    else{
      star.value = "*";
    }
    star.size = star.size/2;
    star.activated = true;
  }
  isvalid(): boolean{
    return this.username!="" && this.password!="";
  }
}
