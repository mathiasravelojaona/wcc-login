import {Component, OnInit, ViewChild} from "@angular/core";
import {SwiperOptions} from "swiper";
import {SwiperComponent} from "ngx-useful-swiper";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MustMatch} from "../../helper/helper";
import {UiStyleToggleService} from "../../theme/UiStyleToggleService";

export enum Brands{
  FACEBOOK,
  INSTAGRAM,
  TWITTER,
  YOUTUBE
}
let brandlist = Brands;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss']
})
export class DashboardPage implements OnInit{
  darkmode: boolean = true;
  brands: Brands = null;
  listebrand = brandlist;
  disableBrands(){
    this.brands = null;
  }
  setBrands(brand: Brands){
    this.brands = brand;
  }
  isFacebook(){
    return this.brands == Brands.FACEBOOK;
  }

  isInstagram(){
    return this.brands == Brands.INSTAGRAM;
  }

  isTwitter(){
    return this.brands == Brands.TWITTER;
  }

  isYoutube(){
    return this.brands == Brands.YOUTUBE;
  }


  ngOnInit(): void {
  }
  public constructor(private uitoggle: UiStyleToggleService){
    this.darkmode = this.uitoggle.isDarkThemeSelected();
  }
  setTheme(valeur){
    this.darkmode = valeur;
    this.uitoggle.toggle();
  }
}
