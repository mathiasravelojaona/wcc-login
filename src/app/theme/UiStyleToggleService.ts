import {Inject, Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs";
import {SESSION_STORAGE, StorageService} from "ngx-webstorage-service";

export enum ThemeMode{
  DARK, LIGHT
}

@Injectable({
  providedIn: 'root'
})
export class UiStyleToggleService {
  private readonly THEME_KEY = 'THEME';
  private readonly DARK_THEME_VALUE = 'DARK';
  private readonly LIGHT_THEME_VALUE = 'LIGHT';
  private readonly LIGHT_THEME_CLASS = 'theme-light';
  private readonly DARK_THEME_CLASS = 'theme-dark';
  private darkThemeSelected = false;
  public theme$ = new BehaviorSubject<ThemeMode>(ThemeMode.DARK);

  constructor(@Inject(SESSION_STORAGE)private storage: StorageService){

  }
  public setThemeOnStart(){
    if(this.isLightThemeSelected()){
      this.setLightTheme();
    } else{
      this.setDarkTheme();
    }
    setTimeout(()=> {
      document.body.classList.add('animate-colors-transition');
    }, 500);
  }
  public toggle(){
    if(this.darkThemeSelected){
      this.setLightTheme();
    }
    else{
      this.setDarkTheme();
    }
  }
  public isDarkThemeSelected(): boolean{
    this.darkThemeSelected = this.storage.get(this.THEME_KEY) === this.DARK_THEME_VALUE;
    return this.darkThemeSelected;
  }
  private isLightThemeSelected(): boolean{
    this.darkThemeSelected = this.storage.get(this.THEME_KEY) === this.LIGHT_THEME_VALUE;
    return this.darkThemeSelected;
  }
  private setLightTheme(){
    this.storage.set(this.THEME_KEY, this.LIGHT_THEME_VALUE);
    document.body.classList.remove(this.DARK_THEME_CLASS);
    document.body.classList.add(this.LIGHT_THEME_CLASS);
    this.darkThemeSelected = false;
    this.theme$.next(ThemeMode.LIGHT);
    console.log("theme changed to light");
  }
  private setDarkTheme(){
    this.storage.set(this.THEME_KEY, this.DARK_THEME_VALUE);
    document.body.classList.add(this.DARK_THEME_CLASS);
    document.body.classList.remove(this.LIGHT_THEME_CLASS);
    this.darkThemeSelected = true;
    this.theme$.next(ThemeMode.DARK);
    console.log("theme changed to dark");
  }
}
